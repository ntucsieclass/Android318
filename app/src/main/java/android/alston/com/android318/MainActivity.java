package android.alston.com.android318;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clickHwBtn (View view){
        Intent intent = new Intent(this,HWActivity.class);
        startActivity(intent);
        Log.d("@@@@@@@","click HW Btn.");
    }

    public void clickIOBtn (View view){
        Intent intent = new Intent(this,IOActivity.class);
        startActivity(intent);
    }

    public void clickPerBtn (View view){
        Intent intent = new Intent(this,PermissionActivity.class);
        startActivity(intent);
    }

    public void clickADPBtn (View view){
        Intent intent = new Intent(this,ADPActivity.class);
        startActivity(intent);
    }

    public void clickMusocBtn (View view){
        Intent intent = new Intent(this,MusicActivity.class);
        startActivity(intent);
    }

    public void clickLifeBtn (View view){
        Intent intent = new Intent(this,LCActivity.class);
        startActivity(intent);
    }

    public void clickRotBtn (View view){
        Intent intent = new Intent(this,RotActivity.class);
        startActivity(intent);
    }

    public void clickTimerBtn (View view){
        Intent intent = new Intent(this,TimerActivity.class);
        startActivity(intent);
    }

    public void clickServiceBtn (View view){
        Intent intent = new Intent(this,SerActivity.class);
        startActivity(intent);
    }

    public void clickCalBtn (View view){
        Intent intent = new Intent(this,CalActivity.class);
        startActivity(intent);
    }
}
