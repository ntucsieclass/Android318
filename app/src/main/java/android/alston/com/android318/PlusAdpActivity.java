package android.alston.com.android318;


import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.*;

public class PlusAdpActivity extends AppCompatActivity {

    private final int REQUEST_CONTACTS = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plus_adp);
        int permission = ActivityCompat.checkSelfPermission(this, READ_CONTACTS);
        if(permission == PackageManager.PERMISSION_GRANTED) {
            read_contact();
        }else {
            String[] request_permission = new String[]{READ_CONTACTS,WRITE_CONTACTS};
            ActivityCompat.requestPermissions(this,request_permission, REQUEST_CONTACTS);
        }
    }
    private void read_contact(){
        ListView listView = (ListView)findViewById(R.id.plusLV);
        List<Map<String,String>> conList = new ArrayList<Map<String,String>>();
        Map<String,String> conMap;

        ContentResolver resolver = getContentResolver();
        Cursor cursor = resolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                null );
        while(cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            String name = cursor.getString(
                    cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            Cursor phoneCursor = resolver.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID+"="+id,
                    null,
                    null);
            String phone = null;
            while(phoneCursor.moveToNext()) {
                phone = phoneCursor.getString(
                            phoneCursor.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER)
                        );
            }
            conMap = new HashMap<String, String>();
            conMap.put("DName",name);
            conMap.put("Phone",phone);
            conList.add(conMap);
        }
        cursor.close();

        SimpleAdapter simpleAdapter = new SimpleAdapter(
                this,
                conList,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{"DName","Phone"},
                new int[]{android.R.id.text1,android.R.id.text2}
        );
        listView.setAdapter(simpleAdapter);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CONTACTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    read_contact();
                else
                    new AlertDialog.Builder(this)
                            .setMessage("拜託請給我權限")
                            .setPositiveButton("拜託啦～～",null)
                            .show();
                break;
            default:
                break;
        }
    }
}
