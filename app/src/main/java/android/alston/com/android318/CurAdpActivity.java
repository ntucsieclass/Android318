package android.alston.com.android318;
        import android.Manifest;
        import android.content.ContentResolver;
        import android.content.pm.PackageManager;
        import android.database.Cursor;
        import android.provider.ContactsContract;
        import android.support.annotation.NonNull;
        import android.support.v4.app.ActivityCompat;
        import android.support.v7.app.AlertDialog;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.widget.ListView;
        import android.widget.SimpleCursorAdapter;
        import android.widget.TextView;
        import android.widget.Toast;

        import static android.Manifest.permission.*;

public class CurAdpActivity extends AppCompatActivity {
    private final int REQUEST_CONTACTS = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cur_adp);
        int permission = ActivityCompat.checkSelfPermission(this, READ_CONTACTS);
        if(permission == PackageManager.PERMISSION_GRANTED) {
            read_contact();
        }else {
            String[] request_permission = new String[]{READ_CONTACTS,WRITE_CONTACTS};
            ActivityCompat.requestPermissions(this,request_permission, REQUEST_CONTACTS);
        }
    }
    private void read_contact(){
        ContentResolver resolver = getContentResolver();
        Cursor cursor = resolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                null );

        ListView listView = (ListView)findViewById(R.id.contactLV);
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_1,
                cursor,
                new String[]{ContactsContract.Contacts.DISPLAY_NAME},
                new int[]{android.R.id.text1},
                1
        );
        listView.setAdapter(simpleCursorAdapter);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CONTACTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    read_contact();
                else
                    new AlertDialog.Builder(this)
                            .setMessage("拜託請給我權限")
                            .setPositiveButton("拜託啦～～",null)
                            .show();
                break;
            default:
                break;
        }
    }
}
