package android.alston.com.android318;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ArrAdpActivity extends AppCompatActivity {
    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listView = new ListView(this);

        ArrayAdapter arrAPT = new ArrayAdapter(
                this,android.R.layout.simple_expandable_list_item_1,getData());
        listView.setAdapter(arrAPT);

        setContentView(listView);
    }

    private List<String> getData(){
        List<String> testdata = new ArrayList<String>();
        testdata.add("測試資料1");
        testdata.add("測試資料2");
        testdata.add("測試資料3");
        return testdata;
    }
}
