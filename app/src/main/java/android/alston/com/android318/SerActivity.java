package android.alston.com.android318;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.Date;

public class SerActivity extends AppCompatActivity {
    private MyService myService = null;
    boolean bound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ser);
    }

    private ServiceConnection mSerCon = new ServiceConnection(){
        @Override
        public void onServiceConnected(ComponentName name, IBinder iBrabcdefg) {
            myService = ((MyService.LoBinder)iBrabcdefg).getService();
            bound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bound = false;
        }
    };


    public void  clickStartBtn(View view){
        Intent intent = new Intent(SerActivity.this,MyService.class);
        startService(intent);
    }
    public void  clickStopBtn(View view){
        Intent intent = new Intent(SerActivity.this,MyService.class);
        stopService(intent);
    }
    public void  clickBindBtn(View view){
        Intent intent = new Intent(SerActivity.this,MyService.class);
        bindService(intent,mSerCon,BIND_AUTO_CREATE);
    }
    public void  clickUnBindBtn(View view){
        unbindService(mSerCon);
        bound = false;
    }
    public void  clickMethodBtn(View view){
        if(bound)
            myService.myBusLoc();
    }
}
