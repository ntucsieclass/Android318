package android.alston.com.android318;

import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimAdpActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SimpleAdapter simpleAdapter= new SimpleAdapter(
                this,
                getData(),
                R.layout.simple_lay,
                new String[]{"image","img_name"},
                new int[]{R.id.icon_img,R.id.icon_text});
        setListAdapter(simpleAdapter);
    }
    private List<Map<String,Object>> getData(){
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        Map<String,Object> map =null;

        map= new HashMap<String,Object>();
        map.put("image",R.drawable.google_chrome_icon);
        map.put("img_name","Chrome");
        list.add(map);

        map= new HashMap<String,Object>();
        map.put("image",R.drawable.firefox_icon);
        map.put("img_name","firefox");
        list.add(map);

        map= new HashMap<String,Object>();
        map.put("image",R.drawable.safari_icon);
        map.put("img_name","safari");
        list.add(map);

        return list;
    }
}
