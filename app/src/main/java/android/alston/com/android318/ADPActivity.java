package android.alston.com.android318;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

public class ADPActivity extends AppCompatActivity {
    Button arrBtn,simBtn,curBtn,plusBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adp);
        arrBtn = (Button) findViewById(R.id.arr_btn);
        simBtn = (Button) findViewById(R.id.sim_btn);
        curBtn = (Button) findViewById(R.id.cursor_btn);
        plusBtn = (Button) findViewById(R.id.plus_btn);

        arrBtn.setOnClickListener(switchACT);
        simBtn.setOnClickListener(switchACT);
        curBtn.setOnClickListener(switchACT);
        plusBtn.setOnClickListener(switchACT);
    }

    private Button.OnClickListener switchACT = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            Intent intent =null;
            switch (v.getId()) {
                case R.id.arr_btn:
                    intent = new Intent(v.getContext(), ArrAdpActivity.class);
                    break;
                case R.id.sim_btn:
                    intent = new Intent(v.getContext(), SimAdpActivity.class);
                    break;
                case R.id.cursor_btn:
                    intent = new Intent(v.getContext(), CurAdpActivity.class);
                    break;
                case R.id.plus_btn:
                    intent = new Intent(v.getContext(), PlusAdpActivity.class);
                    break;
            }
            startActivity(intent);
        }
    };
}
