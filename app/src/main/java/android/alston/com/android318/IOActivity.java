package android.alston.com.android318;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class IOActivity extends AppCompatActivity {
    public static final String EXTRA_NAME ="android.alston.com.android318.IOActivity.EXTRA_NAME";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_io);

    }

    public void clickSendBtn(View view){
        TextView textView = (TextView)findViewById(R.id.namePT);
        String name = textView.getText().toString();
        Log.d("##########","Name:"+name);

        Intent intent = new Intent(this,OutActivity.class);
        intent.putExtra(EXTRA_NAME,name);
        startActivity(intent);
    }
}
