package android.alston.com.android318;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class CalActivity extends AppCompatActivity {

    private Button btn1,btn2,btnC,btnDot,btnPlus,btnAns;
    private TextView screenView;
    String tmpValue,op;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cal);
        btn1 = (Button) findViewById(R.id.btn_1);
        btn2 = (Button) findViewById(R.id.btn_2);
        btnC = (Button) findViewById(R.id.btn_c);
        btnDot = (Button) findViewById(R.id.btn_dot);
        btnPlus = (Button) findViewById(R.id.btn_plus);
        btnAns = (Button) findViewById(R.id.ans_btn);

        screenView = (TextView) findViewById(R.id.calView);

        btn1.setOnClickListener(Number);
        btn2.setOnClickListener(Number);
        btnDot.setOnClickListener(Number);
        //TODO
        btnPlus.setOnClickListener(Operator);
        //TODO
        btnAns.setOnClickListener(ANS);
        btnC.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                screenView.setText("0");
            }
        });
    }

    private Button.OnClickListener Number = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            String oldValue = screenView.getText().toString();
            if(oldValue.equals("0"))
                oldValue="";
            Button tmpBtn = (Button)findViewById(v.getId());
            String numValue = tmpBtn.getText().toString();
            screenView.setText(oldValue+numValue);
        }
    };
    private Button.OnClickListener Operator = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            tmpValue=screenView.getText().toString();
            screenView.setText("");
            switch (v.getId()){
                case R.id.btn_plus:
                {
                    op = "plus";
                    break;
                }
                case R.id.btn_minus: { }
            }
        }
    };
    private Button.OnClickListener ANS = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {
            String nowValue = screenView.getText().toString();
            double nowV = Double.parseDouble(nowValue);
            double tmpV = Double.parseDouble(tmpValue);
            DecimalFormat df = new DecimalFormat("0.00");
            if(op.equals("plus")){
                screenView.setText(df.format(tmpV+nowV));
            }else if (op.equals("min")){

            }

        }
    };
}
