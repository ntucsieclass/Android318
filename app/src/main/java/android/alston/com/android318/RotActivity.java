package android.alston.com.android318;

import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class RotActivity extends AppCompatActivity {
    TextView rotView;
    int number =0;
    DisplayMetrics dm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dm = getResources().getDisplayMetrics();
        int h = dm.heightPixels;
        int w = dm.widthPixels;
        if(h>w) {
            setContentView(R.layout.activity_rot1);
            rotView = (TextView) findViewById(R.id.rotTV1);
        }else {
            setContentView(R.layout.activity_rot2);
            rotView = (TextView) findViewById(R.id.rotTV2);
        }
        Toast.makeText(this,"onCreate",Toast.LENGTH_SHORT).show();
    }

    public void clickCABTN(View view){
        number++;
        rotView.setText(Integer.toString(number));
        Toast.makeText(this,"clickCABTN",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        dm = getResources().getDisplayMetrics();
        int h = dm.heightPixels;
        int w = dm.widthPixels;
        if(h>w) {
            setContentView(R.layout.activity_rot1);
            rotView = (TextView) findViewById(R.id.rotTV1);
        }else {
            setContentView(R.layout.activity_rot2);
            rotView = (TextView) findViewById(R.id.rotTV2);
        }

        rotView.setText(Integer.toString(number));
        Toast.makeText(this,"onConfigurationChanged",Toast.LENGTH_SHORT).show();
    }



    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this,"onStart",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this,"onResume",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Toast.makeText(this,"onRestart",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this,"onPause",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this,"onStop",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this,"onDestroy",Toast.LENGTH_SHORT).show();
    }
}
