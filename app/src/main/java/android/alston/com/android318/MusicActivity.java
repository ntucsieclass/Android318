package android.alston.com.android318;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class MusicActivity extends AppCompatActivity {
    MediaPlayer mediaPlayer;
    Button act_btn;
    String status = "Stop";
    List<Integer> songList;
    int index = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        act_btn = (Button) findViewById(R.id.actionBTN);
        songList = new ArrayList<Integer>();
        songList.add(R.raw.jazz_mango);
        songList.add(R.raw.beach);
        songList.add(R.raw.take_me_to_the_depths);
        mediaPlayer = MediaPlayer.create(this,songList.get(index));
    }

    public void clickAction (View view){
        if(!status.equals("Play")) {
            mediaPlayer.start();
            status="Play";
            act_btn.setText("Pause");
        }else {
            mediaPlayer.pause();
            status="Pause";
            act_btn.setText("Continue");
        }
    }

    public void clickStop (View view){
        mediaPlayer.pause();
        mediaPlayer.seekTo(0);
        status = "Stop";
        act_btn.setText("Play");
    }

    public void clickPre (View view){
        if(index!=0)
            index --;
        else
            index = songList.size()-1;
        mediaPlayer.stop();
        mediaPlayer = MediaPlayer.create(this,songList.get(index));
        mediaPlayer.start();
        status="Play";
        act_btn.setText("Pause");
    }

    public void clickNext (View view){
        if(index != songList.size()-1)
            index ++;
        else
            index = 0;
        mediaPlayer.stop();
        mediaPlayer = MediaPlayer.create(this,songList.get(index));
        mediaPlayer.start();
        status="Play";
        act_btn.setText("Pause");
    }
}
