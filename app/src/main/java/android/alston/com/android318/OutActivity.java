package android.alston.com.android318;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class OutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out);

        Intent abc = getIntent();
        String outputName = abc.getStringExtra(IOActivity.EXTRA_NAME);
        Log.d("%%%%%%%%%%%%%%%",outputName);
        TextView outtext = (TextView)findViewById(R.id.outText);
        outtext.setText("Hi "+outputName);
    }
}
