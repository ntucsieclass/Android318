package android.alston.com.android318;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class TimerActivity extends AppCompatActivity {
    TextView textView;
    Handler hd;
    Thread ct;
    String threadMsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        textView = (TextView)findViewById(R.id.countVW);

        hd = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
//                textView.setText(Integer.toString(msg.arg1));
                textView.setText(threadMsg);
            }
        };
    }
    public void clickStartTimer (View view){
        ct = new Counter();
        ct.start();
    }
    public void clickStopTimer (View view){
        ct.interrupt();
    }

    class Counter extends Thread{
        @Override
        public void run() {
            super.run();
            int i=0;
            int j=0;
            int k=0;
            while(true)
            {
                try {
//                    Message msg = new Message();
//                    msg.arg1=i;
//                    hd.sendMessage(msg);
                    threadMsg=Integer.toString(i)+"."+Integer.toString(j)+Integer.toString(k);
                    hd.sendEmptyMessage(0);
                    Thread.sleep(10);
                    k=k+1;
                    if(k==10){
                        j=j+1;
                        k=0;
                    }
                    if(j==10){
                        i=i+1;
                        j=0;
                    }
                } catch (InterruptedException e) {
                    threadMsg="0.00";
                    hd.sendEmptyMessage(0);
                    break;
                }
            }
        }
    }
}
